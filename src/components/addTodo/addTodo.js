import React, { useState } from "react";
import classNames from "./addTodo.module.css";
const AddTodo = ({add, close}) => {
    const [text, setText] = useState("");
  return (
    <div className={classNames.container}>
      <div className={classNames.box}>
        <div style={{ textAlign: "right" }}>
          <img src="/assets/images/closeadd.png" alt="cancel" />
        </div>
        <div style={{ maxWidth: "90%", margin: "auto" }}>
          <h1
            style={{
              color: "#ff503f",
              fontSize: 24
            }}
          >
            Yeni Görev Ekle
            <div>
              <textarea
                rows={5}
                value={text}
                onChange={e=>setText(e.target.value)}
                style={{
                  width: "100%",
                  borderRadius: 5,
                  padding: 30,
                  fontSize: 20,
                  boxSizing: "border-box",
                  borderColor: "#A4A4A4",
                  outline: "none",
                  marginTop: 16
                }}
              />
            </div>
            <div className={classNames.add} onClick={()=> add({text, completed:false})}>EKLE</div>
          </h1>
        </div>
      </div>
    </div>
  );
};
export default AddTodo;
