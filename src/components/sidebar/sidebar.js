import React from "react";
import classNames from "./sidebar.module.css";

const Sidebar = () => (
  <div className={classNames.sidebar}>
    <div className={classNames.avatar}>
      <img src="/assets/images/avatar.png" alt="avatar" />
    </div>
    <div className={classNames.pages_list}>
      <div
        className={[
          classNames.link_item,
          classNames.active
        ].join(" ")}
      >
        <div className={classNames.badge}>
          <img src="/assets/images/bars.png" alt="bars" />
        </div>
        <div className={classNames.title}>Görevler</div>
      </div>
      <div className={[classNames.link_item, classNames.passive].join(" ")}>
        <img src="/assets/images/settings.png" alt="bars" />
        <div className={classNames.title}>Ayarlar</div>
      </div>
    </div>
    <div className={[classNames.link_item].join(" ")}>
      <img src="/assets/images/close.png" alt="bars" />
      <div className={classNames.title}>Kapat</div>
    </div>
  </div>
);

export default Sidebar;
