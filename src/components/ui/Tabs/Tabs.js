import React from "react";
import classNames from "./Tabs.module.css";

const Tabs = ({
  activeIndex = 2,
  onActiveIndexChange,
  activeTodo = 0,
  children
}) => {
  return (
    <div className={classNames.tabs}>
      <div
        onClick={()=> onActiveIndexChange(0)}
        className={[
          classNames.tab,
          activeIndex === 0 ? classNames.active : ""
        ].join(" ")}
      >
        <img
          className={classNames.icon}
          src="/assets/images/all.png"
          alt="all"
        />
        <div className={classNames.title}>Tüm Görevler</div>
      </div>
      <div
      onClick={()=> onActiveIndexChange(1)}
        className={[
          classNames.tab,
          activeIndex === 1 ? classNames.active : ""
        ].join(" ")}
      >
        <img
          className={classNames.icon}
          src="/assets/images/active.png"
          alt="active"
        />
        <div className={classNames.title}>Aktif Görevler</div>
        <div className={classNames.badge}>{activeTodo}</div>
      </div>
      <div
      onClick={()=> onActiveIndexChange(2)}
        className={[
          classNames.tab,
          activeIndex === 2 ? classNames.active : ""
        ].join(" ")}
      >
        <img
          className={classNames.icon}
          src="/assets/images/completed.png"
          alt="completed"
        />
        <div className={classNames.title}>Biten Görevler</div>
      </div>
      <div className={classNames.fill} />
    </div>
  );
};
export default Tabs;
