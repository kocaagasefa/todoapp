import React from 'react';
import classNames from './checkbox.module.css';
const Checkbox = ({checked, onChange }) => (
    <div className={classNames.checkbox} onClick ={onChange}>
        {checked&& <img src="/assets/images/tick.png" alt="tick"/>}
    </div>
);

export default Checkbox;