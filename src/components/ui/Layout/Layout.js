import React from 'react';
import classNames from './Layout.module.css';
import Sidebar from '../../sidebar/sidebar';
import Header from '../../header/header';

const Layout = ({children}) => (
    <div className={classNames.layout}>
        <Sidebar />
        <div className={classNames.main}>
            <Header />
            {children}
        </div>
    </div>
);

export default Layout;