import React from "react";
import classNames from "./header.module.css";

const Header = () => (
  <div className={classNames.header}>
    <img src="/assets/images/refresh.png" alt="refresh" />
    <div className={classNames.title}>Görev Yönetim Paneli</div>
  </div>
);

export default Header;
