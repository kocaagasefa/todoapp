import React from "react";
import classNames from "./todo.module.css";
import Checkbox from "../ui/checkbox/checkbox";
import Delete from "./buttons/delete";
import Arrow from "./buttons/arrow";

const Todo = ({ todo, onDelete, onDelay, onComplete }) => {
  return (
    <div className={classNames.todo}>
      <Checkbox checked={todo.completed} onChange={() => onComplete(todo.id)} />
      <div
        className={[
          classNames.text,
          todo.completed ? classNames.completed : ""
        ].join(" ")}
      >
        {todo.text}
      </div>
      <div className={classNames.icons}>
        <Delete onClick={() => onDelete(todo.id)} />
        <Arrow onClick={() => onDelay(todo.id)} />
      </div>
    </div>
  );
};
export default Todo;
