import React from "react";
import classNames from "./buttons.module.css";
const Arrow = ({ onClick }) => (
  <div className={classNames.container} onClick={onClick}>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      width="34"
      height="35"
      viewBox="0 0 34 35"
    >
      <defs>
        <filter
          id="a"
          x="0"
          y="0"
          width="34"
          height="35"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="1" input="SourceAlpha" />
          <feGaussianBlur result="b" />
          <feFlood flood-color="#fff" flood-opacity="0.522" />
          <feComposite operator="in" in2="b" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g transform="translate(-1296 -328)">
        <g class="e" transform="matrix(1, 0, 0, 1, 1296, 328)">
          <g class={classNames.outline}>
            <circle class="c" cx="17" cy="17" r="17" />
            <circle class="d" cx="17" cy="17" r="16.5" />
          </g>
        </g>
        <g transform="matrix(0, -1, 1, 0, 1307.572, 348)">
          <path d="M5,10.286a1.091,1.091,0,0,0,.768-.314,1.06,1.06,0,0,0,0-1.515l-3.144-3.1,3.144-3.1a1.061,1.061,0,0,0,0-1.515,1.1,1.1,0,0,0-1.537,0L.318,4.6a1.061,1.061,0,0,0,0,1.515L4.231,9.972A1.091,1.091,0,0,0,5,10.286Z" />
        </g>
        <path
          class={classNames.icon}
          d="M1312.171,347.682l-3.857-3.913a1.1,1.1,0,0,1,0-1.537,1.061,1.061,0,0,1,1.516,0l3.1,3.145,3.1-3.145a1.061,1.061,0,0,1,1.516,0,1.1,1.1,0,0,1,0,1.537l-3.857,3.913a1.059,1.059,0,0,1-1.515,0Z"
        />
      </g>
    </svg>
  </div>
);
export default Arrow;
