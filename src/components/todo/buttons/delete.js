import React from "react";
import classNames from "./buttons.module.css";
const Delete = ({onClick}) => (
  <div className={classNames.container} onClick={onClick}>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="34"
      height="34"
      viewBox="0 0 34 34"
    >
      <g transform="translate(-1252 -418)">
        <circle
          class={classNames.outline}
          cx="17"
          cy="17"
          r="17"
          transform="translate(1252 418)"
        />
        <path
          class={classNames.icon}
          d="M1265.136,440.392q.028.2.5.5a6.033,6.033,0,0,0,1.4.6,6.43,6.43,0,0,0,1.96.294,6.565,6.565,0,0,0,1.974-.294,5.7,5.7,0,0,0,1.4-.6q.462-.308.49-.5l.756-6.8a11.38,11.38,0,0,1-9.24,0Zm5.488-11.676a1.12,1.12,0,0,0-.98-.364h-1.288a1.215,1.215,0,0,0-.98.364l-.588.672a7.067,7.067,0,0,0-2.212.77q-.9.518-.9.994v.14q0,.812,1.568,1.386a12.55,12.55,0,0,0,7.5,0q1.568-.574,1.568-1.386v-.14q0-.476-.9-.994a7.072,7.072,0,0,0-2.212-.77Zm-1.036,1.316h-1.148l-.9.924h-1.176l1.484-1.764a.489.489,0,0,1,.448-.224h1.428a.545.545,0,0,1,.434.224q.182.224,1.47,1.764h-1.176Z"
        />
      </g>
    </svg>
  </div>
);
export default Delete;
