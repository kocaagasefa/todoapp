import React, { useState } from "react";
import classNames from "./Todos.module.css";
import Todo from "../../components/todo/todo";
import Tabs from "../../components/ui/Tabs/Tabs";
import AddTodo from "../../components/addTodo/addTodo";

const initialTodos = [
  { id: 1, text: "Todo 1", completed: false },
  { id: 2, text: "Todo 2", completed: false },
  { id: 3, text: "Todo 3", completed: false },
  { id: 4, text: "Todo 4", completed: false },
  { id: 5, text: "Todo 5", completed: false }
];

const Todos = () => {
  const [id, setId] = useState(6);
  const [activeIndex, setActiveIndex] = useState(0);
  const [todos, setTodos] = useState(initialTodos);
  const [filter, setFilter] = useState("");
  const [add, setAdd] = useState(false);
  const activeTodo = todos.reduce(
    (total, todo) => (todo.completed ? total : total + 1),
    0
  );
  const filteredTodos = todos.filter(todo =>
    todo.text.toLocaleLowerCase("tr").includes(filter.toLocaleLowerCase("tr"))
    && (
      activeIndex === 0 ||
      (activeIndex === 1 && !todo.completed) ||
      (activeIndex === 2 && todo.completed)
    )
  );
  const addTodo = todo => {
    setTodos([...todos, { ...todo, id }]);
    setId(id + 1);
    setAdd(false);
  };
  const deleteTodo = id => setTodos(todos.filter(todo => todo.id !== id));
  const toggleCompleteTodo = id =>
    setTodos(
      todos.map(todo =>
        todo.id !== id ? todo : { ...todo, completed: !todo.completed }
      )
    );
  const delayTodo = id => {
    const index = todos.findIndex(todo => todo.id === id);
    if (index === todos.length - 1) return;
    const newTodos = [...todos];
    [newTodos[index], newTodos[index + 1]] = [
      newTodos[index + 1],
      newTodos[index]
    ];
    setTodos(newTodos);
  };
  return (
    <div className={classNames.container}>
      <Tabs
        activeIndex={activeIndex}
        onActiveIndexChange={setActiveIndex}
        activeTodo={activeTodo}
      />
      <div className={classNames.todos}>
        <div className={classNames.input_container}>
          <input value={filter} placeholder="Arama..." onChange={e => setFilter(e.target.value)} />
          <div className={classNames.add} onClick={()=>setAdd(true)}>Yeni Görev Ekle
            <img src="/assets/images/plus.png" alt="plus" />
          </div>
        </div>
        <div className={classNames.todo_list}>
          {filteredTodos.map(todo => (
            <Todo
              todo={todo}
              key={todo.id}
              onComplete={toggleCompleteTodo}
              onDelay={delayTodo}
              onDelete={deleteTodo}
            />
          ))}
        </div>
        <div className={classNames.select_all}>{`Hepsini Seç    >`}</div>
      </div>
            {add && <AddTodo add={addTodo} close={()=>setAdd(false)}/>}
    </div>
  );
};
export default Todos;
