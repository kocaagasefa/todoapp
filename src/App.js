import React from 'react';
import './App.css';
import Todos from './containers/Todos/Todos';
import Layout from './components/ui/Layout/Layout';

function App() {
  return (
    <Layout>
      <Todos />
    </Layout>
  );
}

export default App;
